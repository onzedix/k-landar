# K-landar

**Décrire les écrans :**
Ecran principal : une liste déroulante pour le mois et une liste pour l’année, un calendrier mensuel (lignes/colonnes - en-têtes de colonne = jours de la semaine). La case du jour est sur-lignée en bleu. Un clic sur une case change la couleur de la case en jaune. Un clic sur la même case enlève la couleur. Un clic sur deux cases change la couleur des deux cases en jaune. Cliquer et déplacer la souris change la couleur des cases traversées selon leur couleur initiale. Cliquer sur la case d’un jour de semaine change la couleur de toutes les cases de la colonne. Sauvegarder les dates sélectionnées dans un tableau pour un usage ultérieur. Animer les sélections et dé-sélections par des micros-animations de couleur.

**Quel est le type d’utilisateur ?**
Un internaute moyen.

**Y-a-t-il des scénarios d'usage spécifiques ?**
Les projet met-il en jeu des algorithmes, des calculs, des processus particuliers ?

**Sauvegarde, transferts et structures de données ?**
Les données des dates sélectionnées sont sauvegardées dans le navigateur.

**Hébergement**
Aucun. Mettre à disposition un sous-domaine de onzedix.fr

**Technologies envisagées**
Compte tenu de la faible complexité du projet, tout sera réalisé en HTML/CSS/Javascript

**Appareils compatibles**
Chrome (5 versions), Edge (dernier), Firefox (dernier), Safari (dernier), iOS10+, Android (Lollilop)

**Design, UX Design, Ergonomie**
S’appuyer sur les calendriers types des OS pour faciliter la compréhension de l’utilisateur, le cercle/disque pour la sélection, centrer sur le calendrier et ajouter un texte d’aide pour faciliter la compréhension des actions possibles.

**Validation du projet, livrables**
Validation par le client. Sources sur Bitbucket et livraison d’une archive en plus des fichiers mis en ligne sur un espace de test.

*Etablir la liste des besoins matériels et numériques qui doivent être fournis par le client*

Merci à https://github.com/dbushell/Pikaday