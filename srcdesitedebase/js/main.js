"use strict";

var klandar = (function () {
    var picker = null;
    var thefield = null;
    var theparent = null;
    var dates = [];
    var weekdays_selected = [false, false, false, false, false, false, false];
    var once = false;

    // la date est-elle dans le tableau des dates
    function exist(a_date) {
        return (indexOf(a_date) !== -1);
    }

    // l'index de la date dans le tableau de dates
    function indexOf(a_date) {
        var index = _.findIndex(dates, function (o) {
            return moment(o).isSame(a_date);
        });
        return index;
    }

    // suppression d'une date du tableau
    function remove(a_date) {
        var index = indexOf(a_date);
        if (index !== -1) {
            _.pullAt(dates, index);
        }
    }

    // on pousse la date dans le tableau
    function add(a_date) {
        dates.push(a_date);
    }

    // un toggle d'une date
    // remise à zéro des jours de semaine sélectionnés pour pouvoir resélectionner la
    // totalité de la colonne
    function addOrRemove(a_date) {
        weekdays_selected[moment(a_date).weekday()] = false;
        if (exist(a_date)) {
            remove(a_date);
        } else {
            add(a_date);
        }
    }

    // un toggle d'une date SUITE A SELECTION D'UN JOUR DE SEMAINE
    // on les sélectionne tous, et on laisse ceux sélectionnés
    // ou
    // on désélectionne tout
    function addOrRemoveButton(b, is_selected) {
        var d = new Date(b.getAttribute('data-pika-year'), b.getAttribute('data-pika-month'), b.getAttribute('data-pika-day'));
        if (is_selected) {
            if (!exist(d)) {
                add(d);
            }
        } else {
            remove(d);
        }
    }

    // draw = classe is-selected, undraw = remove cette classe
    function undrawDates() {
        [].forEach.call(document.querySelectorAll(".is-selected"), function (el) {
            el.classList.remove("is-selected");
        });
    }

    // draw = classe is-selected, undraw = remove cette classe
    // on ne prends que les dates du bon mois
    function drawDates(force) {
        undrawDates();
        var i, max, month, day, elt_day;
        for (i = 0, max = dates.length; i < max; i++) {
            day = moment(dates[i]).date();
            month = moment(dates[i]).month();
            if (document.querySelector('button[data-pika-day="' + day + '"][data-pika-month="' + month + '"]')) {
                elt_day = document.querySelector('button[data-pika-day="' + day + '"][data-pika-month="' + month + '"]').parentNode;
                elt_day.classList.add("is-selected");
            }
        }
        setWeekdayEvents(force);
    }

    // events trigger pour les jorus de semaine
    function setWeekdayEvents(force) {
        if (!once || !!force) {
            once = true;
            [].forEach.call(document.querySelectorAll(".weekday"), function (el) {

                function triggerWeekday(ev) {
                    weekdays_selected[1 * el.getAttribute("data-day")] = !weekdays_selected[1 * el.getAttribute("data-day")];
                    [].forEach.call(document.querySelectorAll('button[data-pika-weekday="' + el.getAttribute("data-day") + '"]'), function (elel) {
                        addOrRemoveButton(elel, weekdays_selected[1 * el.getAttribute("data-day")]);
                    });
                    drawDates();
                }

                el.removeEventListener("click", triggerWeekday);
                el.removeEventListener("touchend", triggerWeekday);
                el.addEventListener("click", triggerWeekday);
                el.addEventListener("touchend", triggerWeekday);
            });
        }
    }

    return {
        init: function (c) {
            if (!picker) {
                thefield = document.createElement("input");
                theparent = c.parentNode;
                theparent.appendChild(thefield);
                thefield.style.display = "none";
                picker = new Pikaday({
                    field: thefield,
                    container: c,
                    firstDay: 1,
                    bound: false,
                    showDaysInNextAndPreviousMonths: true,
                    enableSelectionDaysInNextAndPreviousMonths: false,
                    i18n: {
                        previousMonth: 'Mois précédent',
                        nextMonth: 'Mois suivant',
                        months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                        weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                        weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']
                    },
                    onSelect: function (e) {
                        addOrRemove(e);
                        picker.draw();
                    },
                    onDraw: function (e) {
                        drawDates(true);
                    }
                });
            }
            // picker.gotoToday();
            // picker.show();
            setWeekdayEvents();
        }
    };
})();

document.addEventListener("DOMContentLoaded", function () {
    klandar.init(document.getElementById("calendar_wrapper"));
    _.delay(function () {
        document.querySelector("header p").classList.add("dimmed");
        document.querySelector("footer p").classList.add("dimmed2");
    }, 250);
});
